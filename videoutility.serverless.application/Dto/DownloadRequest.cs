﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Videoutility.Serverless.Application
{
    public class DownloadRequest
    {
        [Required]

        [JsonPropertyName("yt_url")]
        public string Url { get; set; }

        [Required]

        [JsonPropertyName("output_url")]
        public string OutputUrl { get; set; }

        [Required]

        [JsonPropertyName("callback_payload")]
        public Dictionary<string, string> CallbackPayload { get; set; }
    }
}