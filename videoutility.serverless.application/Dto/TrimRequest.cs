﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Videoutility.Serverless.Application
{
    public class TrimRequest
    {
        [Required]
        [JsonPropertyName("input_url")]
        public string InputUrl { get; set; }

        [Required]

        [JsonPropertyName("output_url")]
        public string OutputUrl { get; set; }

        [Required]

        [JsonPropertyName("starttime")]
        public string StartTime { get; set; }

        [Required]

        [JsonPropertyName("endtime")]
        public string EndTime { get; set; }

        [Required]

        [JsonPropertyName("callback_payload")]
        public Dictionary<string, string> CallbackPayload { get; set; }

    }
}