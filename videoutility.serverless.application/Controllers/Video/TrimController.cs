﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Videoutility.Serverless.Application.Controllers.Video
{
    [Route("api/video/[controller]")]
    public class TrimController : Controller
    {
        public ILogger Logger { get; }

        public TrimController(ILogger<TrimController> logger)
        {
            Logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] TrimRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var sqsClient = new Amazon.SQS.AmazonSQSClient();

                await sqsClient.SendMessageAsync(Environment.GetEnvironmentVariable("vtrimurl"), System.Text.Json.JsonSerializer.Serialize(request));

                return Ok();
            }
            catch (Exception ex)
            {

                Logger.LogError(ex.ToString());
                throw;
            }

        }
    }
}
