﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Videoutility.Serverless.Application.Controllers.Video
{
    [Route("api/video/[controller]")]
    public class HealthCheckController : Controller
    {
        public ILogger Logger { get; }

        public HealthCheckController(ILogger<HealthCheckController> logger)
        {
            Logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {

            Logger.LogInformation("health ok ");
            return Ok(new { Status = "Healthy"});

        }
    }
}
