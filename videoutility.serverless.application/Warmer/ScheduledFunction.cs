﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Videoutility.Serverless.Application.Warmer
{
    public class ScheduledFunction
    {
        private static readonly HttpClient client = new HttpClient();
        public async Task FunctionHandlerAsync(ILambdaContext context)
        {
            try
            {
                context.Logger.Log("Scheduled job started");
                context.Logger.Log(Environment.GetEnvironmentVariable("url"));
                var response = await client.GetStringAsync(Environment.GetEnvironmentVariable("url") + "/api/video/healthcheck");
                context.Logger.Log(response);
            }
            catch (Exception ex)
            {

                context.Logger.Log(ex.ToString()); ;
            }
        }
    }
}
